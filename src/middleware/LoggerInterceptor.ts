import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { map, Observable } from 'rxjs';
import Logger from 'src/config/Logger';
import { IResponseLogger } from '../interfaces/Logger/IResponseLogger';
import { IRequestLogger } from '../interfaces/Logger/IRequestLogger';

export interface Response<T> {
  response: T;
}

@Injectable()
export class LoggerInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    const req = context.switchToHttp().getRequest();
    const res = context.switchToHttp().getResponse();

    const method: string = req.method;
    const domain: string = req.headers ? req.headers.host : null;
    const endpoint: string = req.url;
    const headers: string = JSON.stringify(req.headers);
    const body: string = JSON.stringify(req.body);
    const requestMessageLog: IRequestLogger = {
      type: `REQUEST`,
      method,
      domain,
      endpoint,
      body,
      headers,
    };
    Logger.info(JSON.stringify(requestMessageLog));

    return next.handle().pipe(
      map((response) => {
        const statusCode: string = res.statusCode;
        const body: string = JSON.stringify(response);
        const header: string = headers;
        const responseMessageLog: IResponseLogger = {
          type: `RESPONSE`,
          body,
          statusCode,
          header,
        };
        Logger.info(JSON.stringify(responseMessageLog));
        return response;
      }),
    );
  }
}
