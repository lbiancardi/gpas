import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core/constants';
import { envFilePathConfiguration } from './config/Environment';
import { BudgetController } from './controllers/BudgetController';
import { LoggerInterceptor } from './middleware/LoggerInterceptor';
import { BudgetService } from './services/BudgetService';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [envFilePathConfiguration()],
      isGlobal: true,
    }),
  ],
  controllers: [BudgetController],
  providers: [
    { provide: APP_INTERCEPTOR, useClass: LoggerInterceptor },
    BudgetService,
  ],
})
export class AppModule {}
