import Logger from './Logger';

export const envFilePathConfiguration = (): string => {
  const env = !process.env.API_ENV ? `.env` : `.env.${process.env.API_ENV}`;
  Logger.debug(JSON.stringify(process.env));

  return env;
};
