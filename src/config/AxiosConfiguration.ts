import axios from 'axios';
import { IRequestLogger } from 'src/interfaces/Logger/IRequestLogger';
import { IResponseLogger } from 'src/interfaces/Logger/IResponseLogger';
import Logger from './Logger';

const InterceptedAxios = axios.create();

InterceptedAxios.interceptors.request.use(function (config) {
  const type = 'API.EXTERNAL.REQUEST';
  const method: string = config.method;
  const domain: string = config.baseURL;
  const endpoint: string = config.url;
  const body: string = JSON.stringify(config.data);
  const headers: string = JSON.stringify(config.headers);

  const requestMessageLog: IRequestLogger = {
    type,
    method,
    domain,
    endpoint,
    body,
    headers,
  };

  Logger.info(JSON.stringify(requestMessageLog));
  return config;
});

InterceptedAxios.interceptors.response.use(
  function (config) {
    const type = 'API.EXTERNAL.RESPONSE';
    const statusCode = String(config.status);
    const body: string = JSON.stringify(config.data);
    const header: string = JSON.stringify(config.headers);

    const responseMessageLog: IResponseLogger = {
      type,
      body,
      statusCode,
      header,
    };
    Logger.info(JSON.stringify(responseMessageLog));
    return config;
  },
  function (error) {
    if (error.response) {
      const type = 'API.EXTERNAL.RESPONSE';
      const statusCode = String(error.response.status);
      const body: string = JSON.stringify(error.response.data);
      const header: string = null;

      const responseMessageLog: IResponseLogger = {
        type,
        body,
        statusCode,
        header,
      };

      Logger.error(JSON.stringify(responseMessageLog));
    }
    return Promise.reject(error);
  },
);

export default InterceptedAxios;
