export interface IRequestLogger {
  type: string;
  method: string;
  domain: string;
  endpoint: string;
  body: string;
  headers: string;
}
