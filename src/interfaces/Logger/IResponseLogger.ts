export interface IResponseLogger {
  type: string;
  body: string;
  statusCode: string;
  header: string;
}
