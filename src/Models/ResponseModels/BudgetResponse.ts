export default class BudgetResponse {
  id: number;
  icon: string;
  expiration: string;
  type: string;
  insured: string;
  date: Date;
  status: string;

  constructor(response: any) {
    this.id = response.id;
    this.icon = response.ramo;
    this.expiration = response.vencimiento;
    this.type = response.insure;
    this.date = new Date(response.fecha);
    this.status = response.estado;
  }

  static create(response: any): BudgetResponse {
    return response.data.map((data) => new BudgetResponse(data));
  }
}
