import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import BudgetResponse from 'src/Models/ResponseModels/BudgetResponse';
import InterceptedAxios from '../config/AxiosConfiguration';

@Injectable()
export class BudgetService {
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {}

  async getBudget(): Promise<BudgetResponse> {
    const response = await InterceptedAxios.get(
      'https://desolate-inlet-66554.herokuapp.com/budget',
    );

    if (!response) {
      throw new HttpException('not found', HttpStatus.NOT_FOUND);
    }

    return BudgetResponse.create(response);
  }
}
