import { Controller, Get, Post } from '@nestjs/common';
import BudgetResponse from 'src/Models/ResponseModels/BudgetResponse';
import { BudgetService } from 'src/services/BudgetService';

@Controller('/budget')
export class BudgetController {
  constructor(private readonly budgetService: BudgetService) {}
  @Post()
  create(): string {
    return 'This action adds a new cat';
  }

  @Get('')
  async getBudget(): Promise<BudgetResponse> {
    return await this.budgetService.getBudget();
  }
}
